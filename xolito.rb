#!/usr/bin/ruby

# Reads MIME message from stdin and outputs first HTML part with
# attached images embedded

require 'mail'
require 'base64'
require 'nokogiri'

$EMBED_PREFIX = 'cid:'
$HTML_PREFIX = 'text/html'
$IMAGE_PREFIX = 'image/'

# reads through message parts and maps image names to image part
# message - the message
# images - the map to add to
def get_images(message, images)
  message.attachments.each { |attach|
    if (not attach.content_type.nil? and
        attach.content_type.start_with? $IMAGE_PREFIX and
        not attach.content_id.nil?)
      content_id = attach.content_id[/<(.*)>/, 1]
      images[content_id] = attach
    end
  }

  if message.multipart?
    message.parts.each { |part|
      get_images(part, images)
    }
  end
end

# takes a mail image and returns string for img src tag
def image_to_src(image)
  type = image.content_type[/(.*?);/, 1]
  encoded = Base64.encode64(image.decoded)
  return "data:#{type};base64,#{encoded}"
end

# Replace images with embedded images when attached and output
# message - the html message part
# images - map from attachment names to image parts
# bodymode - true if should only output body contents wrapped in a div
def put_html(message, images, bodymode)
  doc = Nokogiri::HTML(message.body.decoded)
  doc.css('img').each { |img|
    src = img['src']
    if src.start_with? $EMBED_PREFIX
      content_id = src[/#{$EMBED_PREFIX}(.*)/, 1]
      if not content_id.nil? and images.member? content_id
        img['src'] = image_to_src(images[content_id])
      end
    end
  }

  if bodymode
    body = doc.at_css('body')
    body.name = 'div'
    puts body
  else
    puts doc
  end
end

# reads through message and outputs first HTML part with attached images
# embedded
# message - the message
# images - map from attachment names to image attachments
# bodymode - true if should only output body contents wrapped in a div
# returns true if putted something
def find_and_put_html(message, images, bodymode)
  if not message.multipart?
    if (not message.content_type.nil? and
        message.content_type.start_with? $HTML_PREFIX)
      put_html(message, images, bodymode)
      return true
    end
  else
    message.parts.each { |part|
      if find_and_put_html(part, images, bodymode)
        return true
      end
    }
  end
  return false
end

bodymode = false
if ARGV.length > 0
  bodymode = ARGV[0] == "--body"
end

message = Mail.new(STDIN.read.encode(Encoding::default_external,
                                     invalid: :replace,
                                     undef: :replace))

images = { }
get_images(message, images)

find_and_put_html(message, images, bodymode)

