# Xolito

Reads an HTML email message from STDIN with attached images and outputs
HTML with the images embedded.

Useful for Mutt: you can pipe messages to the command

    xolito.rb > tmp.html; firefox tmp.html

to view all pictures where intended.

## Requires

* [mail](https://github.com/mikel/mail)
* [nokogiri](https://nokogiri.org/)

## Caveat

This is something i threw together at midnight. Standards have not been
respected, and the code could well not be robust.
